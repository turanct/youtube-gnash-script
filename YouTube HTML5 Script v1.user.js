// ==UserScript==
// @name           YouTube HTML5 Script
// @namespace      http://*.youtube.com/watch?v=*
// @description    Enables HTML5 Playback On YouTube
// @include        http://*.youtube.com/watch?v=*
// @include        https://*.youtube.com/watch?v=*
// @grant          none
// ==/UserScript==

// Timeout the execution with 2 seconds to not have youtube override our player
setTimeout(function() {
	// Declare variables
	var turanct_ytid, turanct_ytembed;

	// Get the youtube id for this movie
	turanct_ytid = document.getElementsByTagName("link").item(4).href;
	turanct_ytid = turanct_ytid.replace(/.*watch\?v=(.*)/i, "$1");

	// Recreate the embed code
	turanct_ytembed = "https://www.youtube.com/embed/"+turanct_ytid;
	turanct_ytembed = "<iframe width='640' height='390' src='"+turanct_ytembed+"' frameborder='0' allowfullscreen></iframe>";

	// Assign to the right DOM part
	document.getElementById("player-api").innerHTML = turanct_ytembed;

},
2000);